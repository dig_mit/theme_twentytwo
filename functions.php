<?php
# make sure is_plugin_active is available when the theme is loaded
include_once(ABSPATH.'wp-admin/includes/plugin.php');

function digmit_register_styles()
{
  wp_enqueue_style('digmit_twentytwo_style', get_stylesheet_uri());
  wp_enqueue_style('dashicons');
}
add_action('wp_enqueue_scripts', 'digmit_register_styles');

function load_google_fonts() {
  wp_register_style('googleFonts', 'https://fonts.googleapis.com/css2?family=Roboto:wght@400;700&display=swap');
  wp_enqueue_style( 'googleFonts');
  }
  add_action('wp_print_styles', 'load_google_fonts');

function digmit_register_menus()
{
  register_nav_menus(
    array(
      'main-menu' => __('Hauptmenü'),
      'extra-menu' => __('Zusatzmenü')
    )
  );
}
add_action('init', 'digmit_register_menus');

function wpb_hook_javascript_footer()
{
?>
  <script>
    const height = document.getElementById("main").offsetHeight;
    if (height < 1500) {
      document.getElementById("background-rectangle-4-container").style.display = "none";
    }
    if (height < 1000) {
      document.getElementById("background-rectangle-2-container").style.display = "none";
    }

    function digmitToggleBurgerMenu (event) {
      const burgerMenu = document.getElementById('burger-menu')
      if (burgerMenu.style.display === 'block') {
        burgerMenu.style.display = 'none'
        jQuery('html').css('overflow-y', 'auto')
      } else {
        burgerMenu.style.display = 'block'
        jQuery('html').css('overflow-y', 'hidden')
      }
    }
    document.getElementById('burger-button').onclick = digmitToggleBurgerMenu
    document.getElementById('burger-button-close').onclick = digmitToggleBurgerMenu
  </script>
<?php
}
add_action('wp_footer', 'wpb_hook_javascript_footer');

function digmit_widgets_init() {
  register_sidebar( array(
    'name' => 'Language Switcher Area',
    'id' => 'sidebar-language',
    'before_widget' => '<nav id="%1$s" class="widget %2$s" style="display: inline;">',
		'after_widget'  => '</nav>',
		'before_title'  => '',
		'after_title'   => '',
  ) );
}
add_action( 'widgets_init', 'digmit_widgets_init' );

/**
 * Provide a selector for main topic pages in the page admin screen
 */
function page_admin_topic_filters(){
  global $wpdb;

  $selected = '';
  if(isset($_GET['pages_topic_filter']) && !empty($_GET['pages_topic_filter'])){
    $selected = $_GET['pages_topic_filter'];
    if (!is_numeric($selected)) {
      error_log('Non-numeric value provided for pages_topic_filter: '.sanitize_text_field($selected));
      return;
    }
    $selected = intval($selected);
  }

  $sql = 'SELECT m.post_id, p.post_title FROM wp_postmeta AS m JOIN wp_posts AS p
    ON m.post_id = p.ID
    WHERE meta_value = "templates/main-topic-page.php";
  ';
  $results = $wpdb->get_results( $sql, ARRAY_A );

  $options = '<option value="">All pages</option>'."\n";
  foreach ( $results as $row ) {
    $options .= '<option value="'.$row["post_id"].'"';
    if ($row["post_id"] == $selected) $options .= ' selected="selected"';
    $options .= '>Main topic: '.sanitize_text_field($row["post_title"])."</option>\n";
  }

  echo '
  <select name="pages_topic_filter">
    '.$options.'
  </select>
  ';
}
add_action('restrict_manage_posts','page_admin_topic_filters');

/**
 * Helper function to get a list of all (sub-)children of a specific page.
 *
 * @param int $page_id The ID of the page to look for children in.
 * @param int $max_recursions Number of maximum recursions level to look for sub-children.
 * @return array A list of IDs for all children and sub-children of the page.
 */
function recurse_topic_children($page_id, $max_recursions) {
  global $wpdb;

  if (!is_int($max_recursions)) $max_recursions = 10;

  $sql = "SELECT ID FROM wp_posts WHERE post_parent = %d";
  $results = $wpdb->get_results( $wpdb->prepare($sql, $page_id), ARRAY_A );
  $children = [];
  foreach ($results as $row) {
    $children[] = $row['ID'];
    if ($max_recursions <= 0) {
      $grandchildren = recurse_topic_children($row['ID'], $max_recursions - 1);
      $children = array_merge($children, $grandchildren);
    }
  }
  return $children;
}

/**
 * Apply a filter for main topic pages on the page admin screen
 */
function apply_topic_filter($query){
  global $pagenow, $post_type, $wpdb;

  if($pagenow == 'edit.php' && $post_type == 'page'){
    if (!isset($_GET['pages_topic_filter'])) return;

    $topic_id = $_GET['pages_topic_filter'];
    if (!is_numeric($topic_id)) {
      error_log('Non-numeric value provided for pages_topic_filter: '.sanitize_text_field($topic_id));
      return;
    }
    $topic_id = intval($topic_id);

    // collect all (sub-)children for the current topic page
    $ids = recurse_topic_children($topic_id, 0);
    // add the topic page itself
    $ids[] = $topic_id;
    // and filter query for all pages that have one of these as parents
    $query->query_vars['post_parent__in'] = $ids;
  }
}
add_action('pre_get_posts','apply_topic_filter');

/**
 * Provide a language selector for the page admin screen
 */
function page_admin_language_filters(){
  // this is Polylang specific, so if plugin is not active, just return
  if (!is_plugin_active('polylang/polylang.php')) return;

  global $post_type;
  if($post_type == 'page'){
      $languages = pll_languages_list();

      $selected = '';
      if(isset($_GET['pages_language_filter'])){
          $selected = sanitize_text_field($_GET['pages_language_filter']);
      }

      $options = '<option value="">All languages</option>'."\n";
      foreach ( $languages as $lang ) {
        $options .= '<option value="'.$lang.'"';
        if ($selected == $lang) $options .= ' selected="selected"';
        $options .= '>'.$lang."</option>\n";
      }

      echo '
      <select name="pages_language_filter">
        '.$options.'
      </select>
      ';
  }
}
add_action('restrict_manage_posts','page_admin_language_filters');

/**
 * Apply a language filter on the page admin screen
 */
function apply_language_filter($query){
  // this is Polylang specific, so if plugin is not active, just return
  if (!is_plugin_active('polylang/polylang.php')) return;

  global $pagenow, $post_type, $wpdb;

  if($pagenow == 'edit.php' && $post_type == 'page'){
    if(isset($_GET['pages_language_filter'])){

      // input validation for the language filter - only allow values that are configured
      $filter_lang = sanitize_text_field($_GET['pages_language_filter']);
      if (!in_array($filter_lang, pll_languages_list())) {
        error_log('Invalid parameter for pages_language_filter: '.$filter_lang);
        return;
      }

      if($filter_lang){
        // get all the post IDs for pages in the selected language (so we can later us it in the WP_Query)
        // thanks to the contributors of this stackoverflow thread:
        // https://stackoverflow.com/questions/47618750/query-wordpress-posts-by-polylang-language-on-mysql
        $sql = 'SELECT ID FROM wp_posts WHERE ID IN (
                  SELECT SUBSTRING_INDEX(SUBSTRING_INDEX(description, \'"'.$filter_lang.'";i:\', -1), \';\',1)
                    FROM `wp_term_taxonomy` WHERE
                      `taxonomy` = \'post_translations\' )';
        $results = $wpdb->get_results( $sql, ARRAY_A );
        $post_ids = [];
        foreach ( $results as $row ) {
          array_push($post_ids, $row['ID']);
        }
        $query->query_vars['post__in'] = $post_ids;
      }
    }
  }
}
add_action('pre_get_posts','apply_language_filter');
