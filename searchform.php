<form role="search" method="get" id="search-form-tag" class="searchform" action="/">
  <div class="search-box">
    <?php _e("", "digmit"); ?>
    <input type="text" name="s" id="s">
    <button type="submit" id="searchsubmit"><span class="dashicons dashicons-arrow-right-alt"></span></button>
  </div>
</form>