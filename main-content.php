    <div id="main-container" class="container">
        <?php
        if ( have_posts() ) {

        $i = 0;

        if ( is_search() ) {
          ?>
          <p>Folgende Suchergebnisse wurden gefunden:</p>
          <?php
        }

        while ( have_posts() ) {
            $i++;
            if ( $i > 1 ) {
            echo '<hr class="post-separator styled-separator is-style-wide section-inner" aria-hidden="true" />';
            }
            the_post();

            ?> <article> <?php
            if ( is_search() ) {
              the_title( '<a href="'.get_permalink().'"><h1 class="entry-title">', '</h1></a>' );
            } else {
              the_title( '<div class="entry-title"><div><h1>', '</h1></div><div><span class="dashicons dashicons-printer" onclick="window.print()"></span></div></div>' );
            }
            the_content();
            ?> </article> <?php

        }
        } elseif ( is_search() ) {
        ?>

        <div class="no-search-results-form section-inner thin">

            <h1><?php _e("Keine Suchergebnisse gefunden", "digmit"); ?></h1>
            <p>
            <?php
            _e("Es konnten keine Suchergebnisse gefunden werden. Probiere es mit einem anderen Begriff:", "digmit");
            get_search_form();
            ?>
            </p>

        </div><!-- .no-search-results -->

        <?php
        }
        ?>
    </div>
