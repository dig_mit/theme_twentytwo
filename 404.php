<?php
get_header();
?>

<main id="main">
    <div id="main-container" class="container">
        <article>
            <h1>Seite nicht gefunden!</h1>
            <p>Die Seite, nach der du gesucht hast gibt es leider nicht.</p>
        </article>
    </div>
</main>

<?php
get_footer();