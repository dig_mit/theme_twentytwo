        <footer id="site-footer">
            <div id="footer-content">
                <div id="footer-content-container">

                    <div class="background-shapes" style="position: relative; z-index: 0;">
                        <div id="background-rectangle-4-container">
                            <svg>
                                <rect id="background-rectangle-4" x="0" y="0" />
                            </svg>
                        </div>
                        <div id="background-rectangle-3-container">
                            <svg>
                                <rect id="background-rectangle-3" x="0" y="0" />
                            </svg>
                        </div>
                    </div>

                    <div id="footer-text-container">

                        <div class="footer-element">
                            <h2 id="footer-heading-projekt">Ein Projekt von</h2>
                            <div id="lefö-logo-footer">
                                <img id="img-lefoe-logo" src="<?= get_theme_file_uri('assets/images/lefoe-300x126.gif'); ?>" alt="LEFÖ Logo">
                            </div>
                        </div>

                        <div class="footer-element">
                            <h2 id="footer-heading-hauptbuero">Hauptbüro</h2>
                            <p id="footer-p-hauptbuero">
                                Kettenbrückengasse 15/4<br>
                                1050 Wien<br>
                                +43 1 581 18 81<br>
                                <a href="mailto:office@lefoe.at">office@lefoe.at</a>
                            </p>
                        </div>

                        <div class="footer-element">
                            <h2 id="footer-heading-digmit">dig_mit!-Projekt</h2>
                            <p id="footer-p-digmit">
                                <a href="mailto:digmit@lefoe.at">digmit@lefoe.at</a><br>
                                <a href="https://www.digmit.at">www.digmit.at</a>
                            </p>
                        </div>

                        <div class="digifonts">
                            <a href="https://wien.arbeiterkammer.at/digifonds" target="_blank">
                                <img id="img-digifonds-logo" src="<?= get_theme_file_uri('assets/images/digifonds-300x300.jpg'); ?>" alt="Gefördert durch Digifonds - AK Wien">
                            </a>
                        </div>

                        <div class="footer-element">
                            <h2></h2>
                            <div class="footer-links-notes">
                                <nav id="footer-links">
                                    <a href="/index.php/impressum-datenschutz/" target="_blank">Impressum</a> |
                                    <a href="/index.php/datenschutz/" target="_blank">Datenschutz</a>
                                </nav>
                                <aside id="footer-notes">
                                    <p> Web development by<br>
                                        <a href="https://tantemalkah.at/" target="_blank"> Tante Malkah</a> &amp;
                                        <a href="https://www.linkedin.com/in/katharinasimma/" target="_blank">Katharina Simma</a>
                                        <br>
                                        Design by
                                        <a href="http://www.ivonnebarrera.cl/" target="_blank"> Ivonne Barrera Villanueva</a>
                                        <br>
                                        Front page images by
                                        <a href="http://www.conmostaza.com/" target="_blank"> Lym Moreno</a>
                                    </p>
                                    <p>
                                        Powered by
                                        <a href="https://wordpress.org/" target="_blank"> WordPress</a>
                                    </p>
                                </aside>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <aside class="print-only">
                <div>
                    <div>
                        <img src="<?= get_theme_file_uri('assets/images/dig_mit-230x278.png'); ?>">
                    </div>
                    <div>
                        <img src="<?= get_theme_file_uri('assets/images/digifonds-300x300.jpg'); ?>">
                        <img src="<?= get_theme_file_uri('assets/images/lefoe-300x126.gif'); ?>">
                    </div>
                </div>
            </aside>
            <?php wp_footer(); ?>
        </footer>
    </body>
</html>