<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
  <title><?php bloginfo('name'); ?> - <?php bloginfo('description'); ?></title>
  <meta charset="<?php bloginfo('charset'); ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="profile" href="https://gmpg.org/xfn/11">

  <?php wp_head(); ?>

  <link rel="stylesheet" type="text/css" media="screen, print" href="<?php bloginfo('stylesheet_directory'); ?>/print.css" />
</head>

<body>

  <nav id="burger-menu" class="w3-modal">
      <div class="w3-modal-content">
        <div class="w3-container">
          <span
            id="burger-button-close"
            class="w3-button w3-display-topright dashicons dashicons-no">
          </span>
          <div id="burger-menu-box">
            <?php
              wp_nav_menu([
                'theme_location' => 'extra-menu',
                'container_id' => 'burger-menu-items'
              ]);
            ?>
          <div class="burger-search">
            <div class="nav-link" id="search-form">
              Suche
            </div>
            <div class="burger-search-form">
              <?php get_search_form(); ?>
            </div>
          </div>
          <a class="nav-link" href="#site-footer">Kontakt</a>
          <a class="nav-link" href="<?= site_url(); ?>">Home</a>

          </div>
        </div>
      </div>
    </nav>

  <header>

    <section id="logo">
      <a href="<?= site_url(); ?>">
        <img src="<?= get_theme_file_uri('assets/images/dig_mit-230x278.png'); ?>" alt="dig_mit! Logo">
      </a>
      <div class="background-shapes" style="position: relative; z-index: 0;">
        <div id="background-rectangle-1-container">
          <svg>
            <rect id="background-rectangle-1" x="0" y="0" />
          </svg>
        </div>
        <div id="background-rectangle-2-container">
          <svg>
            <rect id="background-rectangle-2" x="0" y="0" />
          </svg>
        </div>
      </div>
    </section>
    <aside class="print-only">www.digmit.at</aside>

    <div id="burger">
      <span class="language-switcher">
        <?php dynamic_sidebar( 'sidebar-language' ); ?>
      </span>
      <button id="burger-button" class="w3-button">
        <span class="dashicons dashicons-menu"></span>
      </button>
    </div>

    <nav id="buttons-menu" class="buttons-menu">
      <?php
      wp_nav_menu([
        'theme_location' => 'extra-menu',
        'container_id' => 'main-nav-items'
      ]);
      ?>
    </nav>

    <div class="header-wide">
      <div id="menu-items-wide">
        <span class="language-switcher">
          <?php dynamic_sidebar( 'sidebar-language' ); ?>
        </span>
        <a class="nav-link" href="<?= site_url(); ?>" aria-label="Startseite">
          <span class="dashicons dashicons-admin-home" aria-hidden="true"></span>
        </a>
        <a class="nav-link" href="#site-footer" aria-label="Zu Kontaktinformationen springen">
          <span class="dashicons dashicons-businesswoman" aria-hidden="true"></span>
        </a>
        <label class="nav-link" id="search-label" for="search-form-checkbox-wide" aria-label="Suchfeld öffnen">
          <span class="dashicons dashicons-search" aria-hidden="true"></span>
        </label>
        <div id="search-form-wide">
          <input type="checkbox" id="search-form-checkbox-wide"/>
          <div id="search-form-box-wide">
            <?php get_search_form(); ?>
          </div>
        </div>
      </div>

        <nav class="buttons-menu buttons-menu-wide">
          <?php
          wp_nav_menu([
            'theme_location' => 'extra-menu',
            'container_id' => 'main-nav-items'
          ]);
          ?>
        </nav>
    </div>
  </header>