<?php
get_header();
?>

<main id="main">
    <?php require(get_template_directory() . '/main-content.php'); ?>
</main>

<?php
get_footer();