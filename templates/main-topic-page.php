<?php
/**
 * Template Name: Main topic page
 */

get_header();
?>

<main id="main" class="topic-page">
    <?php require(get_template_directory() . '/main-content.php'); ?>
</main>

<?php
get_footer();